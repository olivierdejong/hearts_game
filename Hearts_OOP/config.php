<?php

const CONFIG = [

    // Player Names
    'config_names' => ['Player1', 'Player2', 'Player3', 'Player4'],

    // Deck Consists of these figures
    'config_figures' => ['7', '8', '9', '10', 'B', 'V', 'K', 'A'],

    // AI Intelligence Level. Options: 'Random' or 'Strategic'
    'config_strategy' => 'Random',

    // Max Score
    'config_maxScore' => '50',

];