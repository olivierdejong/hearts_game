<?php


namespace Classes;


class Player
{
    private $name;
    private $cards;
    private $score;

    public function __construct($name)
    {
        $this->name = $name;
        $this->cards = [];
        $this->score = 0;
    }

    public function __toString(){
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCards()
    {
        return $this->cards;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setCards($cards)
    {
        $this->cards = $cards;
    }

    public function addCard(Card $card)
    {
        array_push($this->cards, $card);
    }

    public function addScore($score)
    {
        $this->score += $score;
    }
}
