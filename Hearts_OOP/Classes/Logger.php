<?php


namespace Classes;


class Logger
{
    static function displayWelcome()
    {
        self::emptyLine();
        self::displayWelcomeMessages();
        self::emptyLine();
        self::displayHands();
        self::displayPlaySequence();
    }

    static function displayWelcomeMessages()
    {
        $nameList = join(', ', PlayerRepository::instance()->getPlayerNames());

        echo "♥♥ Welcome to this game of HEARTS ♥♥";
        self::emptyLine();
        echo "Starting a game with $nameList!";
    }

    static function displayHands()
    {
        $playerCards = PlayerRepository::instance()->getPlayerCards();

        forEach($playerCards as $name => $cards){
            echo $name . " has been dealt: " . join(' ', $cards) . "\n";
        }

        echo "\n";
    }

    static function displayPlaySequence()
    {
        $playSequence = join(', ', PlayerRepository::instance()->getPlaySequence());

        echo "Play sequence: " . $playSequence . "\n";
        self::lineBreak();
    }

    static function displayRound($round)
    {
        $echoRound = $round+1;

        echo "Round number $echoRound:";
        self::emptyLine();
    }

    static function displayTurnInfo($turn)
    {
        $name = PlayerRepository::instance()->getActivePlayerName();
        $cards = PlayerRepository::instance()->getActivePlayerCards();

        echo $turn+1 . ": " . $name . " (" . join('', $cards) . ") ";
    }

    static function displayMove(Card $pick)
    {
        echo "Plays: " . $pick . "\n";
    }

    static function displayEndRound($loser, $pileValue)
    {
        $point = ($pileValue === 1) ? "point" : "points";

        echo "\n" . $loser . " lost this round.\nAdded $pileValue $point.\n";
        self::lineBreak();
    }

    static function reshuffling()
    {
        echo "Reshuffling Cards!";

        self::emptyLine();
        self::displayHands();
        self::displayScore();
    }

    static function displayScore()
    {
        $scoreList = PlayerRepository::instance()->getPlayerScores();

        echo "Points:\n";

        forEach($scoreList as $name => $score){
            echo $name . ": " . $score . "\n";
        }

        self::lineBreak();

        return $scoreList;
    }

    static function displayEndMessage($roundNumber)
    {
        echo "The Game has finished after $roundNumber rounds!";
        self::emptyLine();

        $scoreList = self::displayScore();
        $losingScore = max(array_values($scoreList));
        $winningScore = min(array_values($scoreList));

        forEach ($scoreList as $key => $value) {
            if ($value === $losingScore) {
                $loser = $key;
            } elseif ($value === $winningScore) {
                $winner = $key;
            }
        }

        echo "$loser loses the game.\n";
        echo "$winner is the winner!";
        self::emptyLine();
    }

    static function emptyLine()
    {
        echo "\n\n";
    }

    static function lineBreak()
    {
        echo "\n--------------------";
        self::emptyLine();
    }
}
