<?php


namespace Classes;


class Turn
{
    private $turnNumber;
    private $pile;
    private $cards;
    private $strategy = CONFIG['config_strategy'];

    public function __construct(Int $turn, Pile $pile)
    {
        $this->turnNumber = $turn;
        $this->pile = $pile;
        $activePlayer = PlayerRepository::instance()->getActivePlayerName();
        $this->cards = PlayerRepository::instance()->getCards($activePlayer);
    }

    public function play()
    {
        Logger::displayTurnInfo($this->turnNumber);
        $card = $this->pickCard();
        $this->playCard($card);
        Logger::displayMove($card);
    }

    private function pickCard()
    {
        return $this->chooseCardFromActivePlayer();
    }

    private function playCard($card)
    {
        PlayerRepository::instance()->removeCardFromActivePlayer($card);
        $this->pile->addCard($card);
    }

    private function chooseCardFromActivePlayer()
    {
        $strategy = $this->strategy;
        if ($strategy === 'Random') {
            $card = $this->pickRandomCard();
        } elseif ($strategy === 'Strategic') {
            $card = $this->pickStrategicCard();
        } else {
            Throw new \Exception("Failed to Choose a Card");
        }

        return $card;
    }

    private function pickRandomCard()
    {
        if ($this->isFirstTurn()){
            return $this->getRandomCard();
        }

        return $this->getRandomMatchedCard();
    }

    private function pickStrategicCard()
    {
        $cards = $this->cards;
        foreach($cards as $card) {
            if ($this->isMatch($card)) {
                $matchedCard = !isset($matchedCard) ? $card : Card::getLowest($card, $matchedCard);
            } elseif ($this->isFirstTurn()) {
                $lowCard = !isset($lowCard) ? $card : Card::getLowest($card, $lowCard);
            } elseif (!isset($highCard)) {
                $highCard = !isset($highCard) ? $card : Card::getHighest($card, $highCard);
            }
        }

        return isset($matchedCard) ? $matchedCard : ($this->isFirstTurn() ? $lowCard : $highCard);
    }

    private function getRandomCard(){
        return $this->cards[array_rand($this->cards)];
    }

    private function getRandomMatchedCard(){
        $matchedCards = $this->getAllMatchedCards();
            if(count($matchedCards)){
                return $matchedCards[array_rand($matchedCards)];
            }

        return $this->cards[array_rand($this->cards)];
    }

    private function getAllMatchedCards()
    {
        $matchedCards = [];

        forEach($this->cards as $card) {
            if ($this->isMatch($card)) {
                array_push($matchedCards, $card);
            }
        }

        return $matchedCards;
    }

    private function isMatch(Card $card)
    {
        $cardSuit = $card->getSuit();
        $matcher = $this->getMatcher();

        return $cardSuit === $matcher;
    }

    private function getMatcher()
    {
        return !$this->turnNumber ? '♥' : $this->pile->getSuitOfFirstCard();
    }

    private function isFirstTurn()
    {
        return !$this->pile->getCards();
    }
}