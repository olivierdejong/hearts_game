<?php


use PHPUnit\Framework\TestCase;
use Classes\Card;
use Classes\Pile;


class PileTest extends TestCase
{
    private $pileOfHearts;
    private $pileWithClubsJack;
    private $pileWithSpadesQueen;
    private $pileWithoutValue;

    protected function setUp(): void
    {
        // Fill pile with hearts. Score: 4
        $pile = new Pile();
        $pile->addCard(new Card('♥', 'V'));
        $pile->addCard(new Card('♥', '4'));
        $pile->addCard(new Card('♥', 'K'));
        $pile->addCard(new Card('♥', '10'));
        $this->pileOfHearts = $pile;

        // Fill pile with Jack of Clubs and one hearts. Score: 3
        $pile = new Pile();
        $pile->addCard(new Card('♦', 'K'));
        $pile->addCard(new Card('♥', '4'));
        $pile->addCard(new Card('♣', 'B'));
        $pile->addCard(new Card('♠', '10'));
        $this->pileWithClubsJack = $pile;

        // Fill pile with Queen of Spades and two hearts. Score: 7
        $pile = new Pile();
        $pile->addCard(new Card('♠', 'V'));
        $pile->addCard(new Card('♥', '4'));
        $pile->addCard(new Card('♣', '2'));
        $pile->addCard(new Card('♥', '10'));
        $this->pileWithSpadesQueen = $pile;

        // Fill pile without value. Score: 0
        $pile = new Pile();
        $pile->addCard(new Card('♣', '4'));
        $pile->addCard(new Card('♦', '4'));
        $pile->addCard(new Card('♠', '2'));
        $pile->addCard(new Card('♦', '10'));
        $this->pileWithoutValue = $pile;
    }

    public function test_GetCorrectValueOfHearts()
    {
        $this->assertEquals(4, $this->pileOfHearts->getValue());
    }

    public function test_GetCorrectValueOfClubsJack()
    {
        $this->assertEquals(3, $this->pileWithClubsJack->getValue());
    }

    public function test_GetCorrectValueOfSpadesQueen()
    {
        $this->assertEquals(7, $this->pileWithSpadesQueen->getValue());
    }

    public function test_GetCorrectValueOfNoneValueCards()
    {
        $this->assertEquals(0, $this->pileWithoutValue->getValue());
    }

    public function test_GetCorrectSuitOfFirstCard()
    {
        $this->assertEquals('♠', $this->pileWithSpadesQueen->getSuitOfFirstCard());
    }

    public function test_GetLosingCardKeyWhenMatched()
    {
        $this->assertEquals(2, $this->pileOfHearts->getLosingCardKey());
    }

    public function test_GetLosingCardKeyWhenNotMatched()
    {
        $this->assertEquals(0, $this->pileWithoutValue->getLosingCardKey());
    }
}