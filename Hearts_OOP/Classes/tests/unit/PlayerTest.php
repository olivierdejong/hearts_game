<?php
use PHPUnit\Framework\TestCase;

use Classes\Card;
use Classes\PlayerRepository;


class PlayerTest extends TestCase
{
    private $player;

    protected function setUp(): void
    {
        PlayerRepository::instance()->createPlayers(['Tester1', 'Tester2', 'Tester3', 'Tester4']);
        PlayerRepository::instance()->shufflePlayers();
        $player = PlayerRepository::instance()->getActivePlayer();
        $player->setCards([
            new Card('♦', '10'),
            new Card('♣','2'),
            new Card('♠', 'A'),
            new Card('♥', 'B'),
        ]);

        $this->player = $player;
    }

    public function test_CanGetCardsFromValidPlayer()
    {
        $player = PlayerRepository::instance()->getActivePlayer();
        $this->assertEquals($player->getCards(), PlayerRepository::instance()->getActivePlayerCards());
    }

    public function test_CanNotGetCardsFromInvalidPlayer()
    {
        $this->expectException(\Exception::class);
        PlayerRepository::instance()->getPlayer('Tester5');
    }

    public function test_CanRemoveCardFromActivePlayer()
    {
        $card = PlayerRepository::instance()->getActivePlayerCards()[0];
        PlayerRepository::instance()->removeCardFromActivePlayer($card);
        $this->assertNotContains($card, PlayerRepository::instance()->getActivePlayerCards());
    }

    public function test_CanNotRemoveInvalidCardFromActivePlayer()
    {
        $card = new Card('♣','4');
        $this->expectException(\Exception::class);
        PlayerRepository::instance()->removeCardFromActivePlayer($card);
    }
}