<?php


namespace Classes;


class PlayerRepository
{
    protected static $instance;
    protected $players = [];
    protected $playerSequence;

    public static function instance()
    {
        if( isset(self::$instance)){
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }


    // =========== Methods on All Players ===========

    public function createPlayers(?array $players = null)
    {
        $nameList = $players ?? CONFIG['config_names'];

        if(count($nameList) !== 4){
            Throw new \Exception('NameList length error: number of players must be 4');
        }

        forEach ($nameList as $name){
            array_push($this->players, new Player($name));
        }

        return $this->players;
    }

    public function shufflePlayers()
    {
        $names = $this->getPlayerNames();
        shuffle($names);
        $this->playerSequence = $names;
    }

    public function shiftPlaySequence()
    {
        array_push($this->playerSequence, array_shift($this->playerSequence));
    }

    public function getPlaySequence()
    {
        if($this->playerSequence){
            return $this->playerSequence;
        }else{
            Throw new \Exception("PlayerSequence has not been set");
        }
    }

    public function getPlayerNames()
    {
        $nameList = [];

        forEach($this->players as $player){
            array_push($nameList, $player->getName());
        }

        return $nameList;
    }

    public function getPlayerCards()
    {
        $cardList = [];

        forEach($this->players as $player){
            $name = $player->getName();
            $cards = $player->getCards();
            $cardList[$name] = $cards;
        }

        return $cardList;
    }

    public function getPlayerScores()
    {
        $scoreList = [];

        forEach($this->players as $player){
            $name = $player->getName();
            $score = $player->getScore();
            $scoreList[$name] = $score;
        }

        return $scoreList;
    }


    // =========== Methods on Specific Players ===========

    public function getPlayer($name)
    {
        forEach($this->players as $player){
            if ($player->getName() === $name){
                return $player;
            }
        }
        throw new \Exception("No Player With name $name");
    }

    public function getCards($name)
    {
        return $this->getPlayer($name)->getCards();
    }

    public function getActivePlayer()
    {
        return $this->getPlayer($this->getPlaySequence()[0]);
    }

    public function getActivePlayerName()
    {
        return $this->getPlaySequence()[0];
    }

    public function getActivePlayerCards()
    {
        return $this->getCards($this->getPlaySequence()[0]);
    }

    public function removeCardFromActivePlayer(Card $card)
    {
        $player = $this->getActivePlayer();
        $cards = $player->getCards();

        if(($key = array_search($card, $cards, $strict = TRUE)) !== FALSE) {
            unset($cards[$key]);
        } else {
            throw new \Exception("$card not found in hand of $player");
        }

        $player->setCards($cards);
    }

    public function addCard($name, $card)
    {
        return $this->getPlayer($name)->addCard($card);
    }

    public function addScore($name, $score)
    {
        return $this->getPlayer($name)->addScore($score);
    }

}