<?php


namespace Classes;

class Deck
{
    private $cards = [];

    public function __construct()
    {
        $figures = CONFIG['config_figures'];

        for($i=0; $i<4; $i++) {
            for ($j = 0; $j < (count($figures)) ; $j++) {
                array_push($this->cards, new Card(Card::SUITS[$i], $figures[$j]));
            }
        }

        $this->shuffleCards();
    }

    private function shuffleCards()
    {
        shuffle($this->cards);
    }

    public function distribute()
    {
        $playerNames = PlayerRepository::instance()->getPlayerNames();

        while (count($this->cards)) {
            foreach ($playerNames as $name) {
                PlayerRepository::instance()->addCard($name, array_pop($this->cards));
            }
        }
    }
}