<?php


namespace Classes;


class Card
{
    const SUITS = ['♣', '♠', '♥', '♦'];
    const VALUE = [
        "2" => 2,
        "3" => 3,
        "4" => 4,
        "5" => 5,
        "6" => 6,
        "7" => 7,
        "8" => 8,
        "9" => 9,
        "10" => 10,
        "B" => 11,
        "V" => 12,
        "K" => 13,
        "A" => 14
    ];

    private $figure;
    private $suit;

    public function __construct($suit, $figure)
    {
        $this->figure = $figure;
        $this->suit = $suit;
    }

    public function __toString()
    {
        return $this->suit . $this->figure;
    }

    public function getFigure()
    {
        return $this->figure;
    }

    public function getSuit()
    {
        return $this->suit;
    }

    public function getValue()
    {
        return self::VALUE[$this->getFigure()];
    }

    static function getHighest(Card $card1, Card $card2)
    {
        if ($card1->getValue() < $card2->getValue()){
            return $card2;
        }

        return $card1;
    }

    static function getLowest(Card $card1, Card $card2)
    {
        if ($card1->getValue() > $card2->getValue()){
            return $card2;
        }

        return $card1;
    }
}