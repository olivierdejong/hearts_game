<?php


namespace Classes;


class Pile
{
    private $cards = [];

    public function addCard(Card $card)
    {
        array_push($this->cards, $card);
    }

    public function getCards()
    {
        return $this->cards;
    }

    public function getSuitOfFirstCard()
    {
        return $this->getFirstCard()->getSuit();
    }

    public function discard(){
        $this->cards = [];
    }

    public function getLosingCardKey()
    {
        $losingCard = $this->getFirstCard();
        $matchSuit = $this->getSuitOfFirstCard();

        forEach($this->cards as $card) {
            if ($card->getSuit() === $matchSuit) {
                $losingCard = Card::getHighest($losingCard, $card);
            }
        }

        return array_search($losingCard, $this->cards);
    }

    public function getValue()
    {
        $pileValue = 0;
        forEach ($this->cards as $card){
            switch ($card){
                case (preg_match( '/♥.*/', $card) ? true : false ):
                    $pointValue = 1;
                    break;
                case "♣B":
                    $pointValue = 2;
                    break;
                case "♠V":
                    $pointValue = 5;
                    break;
                default:
                    $pointValue = 0;
                    break;
            }

            $pileValue += $pointValue;
        }

        return $pileValue;
    }

    private function getFirstCard()
    {
        return $this->getCards()[0];
    }
}