<?php


namespace Classes;


class Game
{
    private $roundNumber = 0;

    public function __construct()
    {
        $this->createPlayers();
        $this->distributeCards();
    }

    private function createPlayers()
    {
        PlayerRepository::instance()->createPlayers();
    }

    private function distributeCards()
    {
        $deck = new Deck();
        $deck->distribute();
    }

    public function initializeGame()
    {
        $this->startGame();
        $this->playRounds();
        $this->endGame();
    }

    private function startGame()
    {
        $this->randomizePlayerSequence();
        $this->displayWelcomeMessages();
    }

    private function playRounds()
    {
        $playRounds = true;
        while ($playRounds) {
            $this->prepareRound();
            $playRounds = $this->playRound();
            $this->endRound();
        }
    }

    private function endGame()
    {
        Logger::displayEndMessage($this->roundNumber);
    }

    private function randomizePlayerSequence()
    {
        PlayerRepository::instance()->shufflePlayers();
    }

    private function displayWelcomeMessages()
    {
        Logger::displayWelcome();
    }

    private function prepareRound(){
        $this->checkIfPlayersHaveCards();
    }

    private function playRound()
    {
        $round = new Round($this->roundNumber);
        return $round->play();
    }

    private function checkIfPlayersHaveCards()
    {
        if ($this->handsAreEmpty()) {
            $this->reshuffleCards();
        }
    }

    private function endRound()
    {
        $this->addRoundNumber();
        $this->setNewStartingPlayer();
    }

    private function addRoundNumber()
    {
        $this->roundNumber++;
    }

    private function setNewStartingPlayer()
    {
        PlayerRepository::instance()->shiftPlaySequence();
    }

    private function handsAreEmpty()
    {
        return !PlayerRepository::instance()->getActivePlayerCards();
    }

    private function reshuffleCards()
    {
        $this->distributeCards();
        Logger::reshuffling();
    }
}