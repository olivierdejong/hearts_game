<?php


namespace Classes;


class Round
{
    private $roundNumber;
    private $playSequence;
    private $pile;
    private $scoreLimit = CONFIG['config_maxScore'];

    public function __construct($roundNumber)
    {
        $this->roundNumber = $roundNumber;
        $this->pile = new Pile();
        $this->playSequence = PlayerRepository::instance()->getPlaySequence();
    }

    public function play()
    {
        $this->displayRound();
        $this->playTurns();
        $this->addScores();
        $this->endRound();

        if ($this->scoreLimitReached()) {
            return false;
        }

        return true;
    }

    private function displayRound()
    {
        Logger::displayRound($this->roundNumber);
    }

    public function playTurns()
    {
        for ($i = 0; $i < count(PlayerRepository::instance()->getPlayerNames()); $i++) {
            $turn = new Turn($i, $this->pile);
            $turn->play();
            $this->nextPlayer();
        }

        return false;
    }

    private function addScores()
    {
        $loser = $this->selectLoser();
        $pileValue = $this->pile->getValue();
        PlayerRepository::instance()->addScore($loser, $pileValue);
    }

    private function selectLoser()
    {
        $losingKey = $this->pile->getLosingCardKey();
        return $this->playSequence[$losingKey];
    }

    private function endRound()
    {
        $this->displayRoundEnd();
        $this->discardPile();
    }

    private function discardPile()
    {
        $this->pile->discard();
    }

    private function displayRoundEnd()
    {
        Logger::displayEndRound($this->selectLoser(), $this->pile->getValue());
    }

    private function scoreLimitReached()
    {
        $scoreList = PlayerRepository::instance()->getPlayerScores();

        forEach ($scoreList as $name => $score) {
            if ($score >= $this->scoreLimit) {
                return true;
            }
        }
        return false;
    }

    private function nextPlayer()
    {
        PlayerRepository::instance()->shiftPlaySequence();
    }
}