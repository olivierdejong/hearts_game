##Game Rules:
1. Selected cards (7 to Ace) are shuffled and distributed among four players.
2. The play sequence is selected randomly.
3. First player must match a ♥ if possible.
4. Other players must match first players card if possible.
5. Player who played highest matched card loses.
6. Loser receives points according to value of all four cards on the table.
7. Next player starts the new turn.
8. When hands are empty, cards are reshuffled and distributed.
9. The first player who hits the 50 point mark, loses.
10. The player with the lowest score wins.
<br><br>

####Remarks regarding the assignment briefing:
-Should first player not try to match hearts?<br>
-Should no other strategic moves be implemented?<br>
-Note that if no player can match the card of first player, first player loses the round.<br>
<br><br>

####Time spent developing the application:
Rewriting code to OOP: 6hrs. <br>
Developing Player Repository, Config file and README: 3hrs. <br>
Building Unit Tests: 3hrs. <br>
<br><br>

####Extra:
1. A config file has been added to the root file. In this file, some constants, like "strategy", can be modified.<br>
"Random" let's all players pick random cards (random matched card if possible)<br>
"Strategic" let's players pick either high or low cards depending on the turn
2. A Player Repository has been added to improve flexibility and availability of Player class.<br>
3. A "Logger" class has been added to deal with console display.
4. Two PHPUnit tests have been added. Run composer install -> vendor/bin/phpunit to run both.