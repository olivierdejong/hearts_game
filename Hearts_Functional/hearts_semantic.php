<?php

// ======================== Initialize Game =======================

if (!isset($testing)){
initializeGame(['Player1', 'Player2', 'Player3', 'Player4']);
}

// ======================== Game Progress Functions =======================


function initializeGame($names){
    createPlayers($names);
    clearTable();
    startGame();
}


function startGame(){
    displayWelcomeMessage();
    shuffleCards();
    randomizeStartingPlayer();
    lineBreak();
    playRounds();
}


function playRounds(){
    $round = 0;
    $next_round = true;

    while ($next_round){
        displayRound($round);
        playerTurns();

        $loser = selectLoser();
        displayLoser($loser);
        addScores($loser);

        if (scoreLimitReached()){
            $next_round = false;
        } else {
            prepareNextRound();
            $round++;
        }
    }

    endGame();
}


function playerTurns(){
    for($i = 0; $i < 4; $i++) {
        $player = getActivePlayer($i);
        displayTurnInfo($i, $player);
        setMove($player);
    }
}


function setMove($player){
    $cards = getCards($player);
    $pick = chooseCard($cards);

    playMove($player, $cards, $pick);
    displayMove($pick);
}


function playMove($player, $cards, $pick){
    $cardKey = selectCard($pick, $cards);

    removeCardFromHand($cardKey, $player);
    addCardToPile($pick);
}


function selectLoser(){
    global $names, $pile;

    $losingCard = getLosingCard();
    $losingKey = array_search($losingCard, $pile);

    return $names[$losingKey];
}


function addScores($loser){
    global $players;

    $pileValue = getPileValue();

    $players[$loser]['score'] += $pileValue;
    echo "Added $pileValue points.\n";

    lineBreak();
}


function prepareNextRound(){
    clearTable();
    getStartingPlayer();

    if (handsAreEmpty()) reshuffleCards();
}


function endGame(){
    displayEndMessage();
    $scoreList = displayPoints();
    displayWinnerAndLoser($scoreList);
}


// ======================== Game Helper Functions =======================


function chooseCard($cards){
    $matcher = getMatcher();

    foreach($cards as $card) {

        // Remember lowest suited card
        if (matchFound($card, $matcher)){
            if (!isset($match)) {
                $match = $card;
            } else {
                $match = getLowestCard($card, $match);
            }

        // Remember lowest unsuited card
        } elseif (isFirstTurn()) {
            if (!isset($lowCard)) {
                $lowCard = $card;
            } else {
                $lowCard = getLowestCard($card, $lowCard);
            }

        // Remember highest unsuited card
        } elseif (!isset($highCard)) {
            $highCard = $card;
        } else {
            $highCard = getHighestCard($card, $highCard);
        }
    }

    // If match is found: play lowest match. Else: first player plays lowcard, others play highcard
    return isset($match) ? $match : (isset($lowCard) ? $lowCard : $highCard);
}


function createPlayers($name_input){
    global $players, $names;

    $players = [];
    $names = $name_input;

    foreach ($names as $name) {
        $players[$name] = ["hand" => [], "score" => 0];
    }
}


function makeCards($shuffle = true){
    global $deck;
    $deck = [];

    $colors = ['♣', '♠', '♥', '♦'];
    $values = array_merge(range(7,10), ['B', 'V', 'K', 'A']);

    for($i=0; $i<4; $i++) {
        for ($j = 0; $j < (count($values)) ; $j++) {
            array_push($deck, $colors[$i] . $values[$j]);
        }
    }

    if($shuffle === true) shuffle($deck);
}


function shuffleCards(){
    makeCards();
    divideCards();
    displayHands();
    displayPoints();
}


function divideCards(){
    global $players, $deck;

    while (count($deck)) {
        foreach ($players as $i => $player) {
            array_push($players[$i]["hand"], array_pop($deck));
        }
    }
}


function reshuffleCards(){
    echo "Reshuffling Cards!\n";

    shuffleCards();
    lineBreak();
}


function addCardToPile($card){
    global $pile;

    array_push($pile, $card);
}


function randomizeStartingPlayer(){
    global $names;

    shuffle($names);

    echo "\n" . $names[0] . " may start the game.\n";
}


function selectCard($pick, $cards){
    return array_search($pick, $cards);
}


function removeCardFromHand($cardKey, $player){
    global $players;

    array_splice($players[$player]["hand"], $cardKey, 1)[0];
}


function clearTable(){
    global $pile;

    $pile = [];
}


// ======================== Boolian / Check Functions =======================



function handsAreEmpty(){
    global $players, $names;

    return !$players[$names[0]]['hand'];
}


function isFirstTurn(){
    global $pile;

    return !$pile;
}


function scoreLimitReached(){
    global $players;

    forEach($players as $name => $info){
        if ($info['score'] >= 50){
            return true;
        }
    }

    return false;
}


function matchFound($card, $matcher){
    $cardColor = getColor($card);

    return $cardColor === $matcher;
}


// ======================== Getter / Setter Functions =======================


function getMatcher(){
    global $pile;

    return isFirstTurn() ? '♥' : mb_substr($pile[0], 0, 1);
}


function getPileValue(){
    global $pile;

    $pileValue = 0;

    forEach ($pile as $card){
        switch ($card){

            // Case that $card starts with a ♥ symbol
            case ( preg_match( '/♥.*/', $card ) ? true : false ):
                $pointValue = 1;
                break;
            case "♣B":
                $pointValue = 2;
                break;
            case "♠V":
                $pointValue = 5;
                break;
            default:
                $pointValue = 0;
                break;
        }

        $pileValue += $pointValue;
    }

    return $pileValue;
}


function getCards($player){
    global $players;

    return $players[$player]["hand"];
}


function getActivePlayer($turn){
    global $names;

    return $names[$turn];
}


function getLosingCard(){
    global $pile;

    $losingCard = $pile[0];
    $matchColor = getColor($pile[0]);

    forEach(array_slice($pile,1) as $card) {
        if (getColor($card) === $matchColor) {
            $losingCard = getHighestCard($losingCard, $card);
        }
    }

    return $losingCard;
}


function getStartingPlayer(){
    global $names;

    array_push($names, array_shift($names));
}


function getColor($card){
    return mb_substr($card, 0, 1);
}


function getValue($card){
    global $values;
    setCardValues();

    $cardValue = mb_substr($card, 1);
    if (!is_numeric($cardValue)){
        $cardValue = $values[$cardValue];
    }

    return $cardValue;
}


function getLowestCard($card1, $card2){
    if (getValue($card1) > getValue($card2)){
        return $card2;
    }

    return $card1;
}


function getHighestCard($card1, $card2){
    if (getValue($card1) < getValue($card2)){
        return $card2;
    }

    return $card1;
}


function setCardValues(){
    global $values;

    $values = [
        "B" => 11,
        "V" => 12,
        "K" => 13,
        "A" => 14
    ];
}


function displayEndMessage(){
    echo "The Game has Finished!\n\n";
}


function displayPoints(){
    global $players;
    $scoreList = [];

    echo "\nPoints:\n";
    forEach($players as $player => $info){
        $score = $info["score"];
        $scoreList[$player] = $score;

        echo "$player: " . $score . "\n";
    }

    return $scoreList;
}


function displayWelcomeMessage(){
    global $names;

    $nameList = join($names, ', ');

    echo "\n♥♥ Welcome to this game of HEARTS ♥♥\n";
    echo "\nStarting a game with $nameList! \n\n";
}


function displayWinnerAndLoser($scoreList){
    $loser = array_search(max(array_values($scoreList)), $scoreList);
    $winner = array_search(min(array_values($scoreList)), $scoreList);

    echo "\n$loser loses the game.\n";
    echo "$winner is the winner!\n\n";
}


function displayHands(){
    global $players;

    $names = array_keys($players);

    for($i=0; $i<4; $i++){
        echo "$names[$i] has been dealt: " . join(' ', $players[$names[$i]]["hand"]) . "\n";
    }
}


function displayLoser($loser){
    echo "\n$loser lost this round!\n";
}


function displayTurn($turn){
    echo $turn+1 . ": ";
}


function displayPlayer($player){
    echo $player;
}


function displayHand($player){
    $cards = getCards($player);
    echo " (" . join($cards, '') . ")";
}


function displayTurnInfo($turn, $player){
    $matcher = getMatcher();
    echo $matcher;

    displayTurn($turn);
    displayPlayer($player);
    displayHand($player);
}


function displayRound($round){
    $echoRound = $round+1;
    echo "round number $echoRound:\n\n";
}


function displayMove($pick){
    echo " Plays:" . $pick . "\n";
}


function lineBreak(){
    echo "\n -------------------- \n\n";
}