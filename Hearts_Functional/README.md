##Game Rules:
1. All cards (7 to Ace) are shuffled and distributed among four players.
2. The starting player is selected randomly.
3. First player must match a ♥ if possible (always plays lowest possible card: strategic move).
4. Other players must match first players card if possible (if match is possible, lowest card is played. Otherwise, highest card is played: strategic move).
5. Player that played highest matched card loses.
6. Loser receives points according to value of all four cards on the table.
7. Next player starts the new turn.
8. When hands are empty, cards are reshuffled and distributed.
9. When a player hits the 50 point mark, he loses.
10. The player with the lowest score wins.
<br>

####Remarks regarding the assignment briefing:
-It is rather unclear whether players should prioritize playing hearts cards, 
or always match the first players' card color<br>
-Regarding the picking of a rounds loser, it is unclear which cards holds more points; 
does an Ace of spades hold more points than a hearts card, because the card value is higher?<br>
-Does the loser of a round get accounted for the cumulative points of the pile, or solely of his own card?<br>
-Will a developer get bonus points for adding strategic moves to the program?<br>
-An extension of the example round, in particular the addition of a "picking loser" and "adding points" step,
would clarify these rules.<br>
<br>
-For this application, when in doubt, the official rules of Hearts have been consulted. See the "Game Rules" 
paragraph for an overview of the implemented rules.
<br>

####Time spent developing the application:
Reading on Hearts rules and setup: 1.5hrs. <br>
Programming functions: 6hrs <br>
Making code more semantic: 3hrs. <br>
Building Tester function: 3hr. <br>