
<!---->

This script tests how the application hearts_semantic.php chooses which card to play.

<!---->
<?php

$testing = true;
include 'hearts_semantic.php';

$cards = explode(" ", '♥K ♠2 ♦8 ♦A ♠10 ♦4 ♠8 ♦K ♥4 ♠V');
$values = [
    "B" => 11,
    "V" => 12,
    "K" => 13,
    "A" => 14
];

$validTestCases = [
    [[], "♥4"],         // When player starts
    [['♦K'], "♦4"],     // When cards contain requested (high card) color
    [['♠4'], "♠2"],     // When cards contain requested (low card) color
    [['♣3'], "♦A"],     // When cards do not contain requested (high card) color
    [['♣A'], "♦A"],     // When cards do not contain requested (low card) color
];

$invalidTestCases = [
    [['➜5'], "♦A"],     // No matcher found: play highest card
    [['4'], "♦A"],      // No matcher found: play highest card
    [[null], "♦A"],     // No matcher found: play highest card
    [['♣99'], "♦A"],    // No matcher found: play highest card
];

inputTester($validTestCases, "valid");
inputTester($invalidTestCases, "invalid");


// ======================== Tester Functions =======================


function inputTester($testCases, $type){
    echo "\nStarting Tester:\n\n";

    if ($type === "valid"){
        runValidTests($testCases);
    } elseif ($type === "invalid") {
        runInvalidTests($testCases);
    }

    echo "\nTester Finished.\n\n";
}


function runValidTests($testCases){
    global $pile, $cards;

    echo "Valid Input Tests:\n";
    $nr = 1;
    forEach ($testCases as $testCase) {
        $pile = $testCase[0];
        $expected = $testCase[1];
        $pick = chooseCard($cards);

        echo $nr . ": ";
        if ($expected === $pick) {
            echo "Test successful\n";
        } else {
            echo "Test failed\n";
        }
        $nr++;
    }
}

function runInvalidTests($testCases){
    global $pile, $cards;

    echo "Invalid Input Tests:\n";
    $nr = 1;
    forEach ($testCases as $testCase) {
        $pile = $testCase[0];
        $expected = $testCase[1];
        $pick = chooseCard($cards);

        echo $nr . ": ";
        if ($expected === $pick) {
            echo "Test successful\n";
        } else {
            echo "Test failed\n";
        }
        $nr++;
    }
}
